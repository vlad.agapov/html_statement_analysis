#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
import numpy as np


# In[3]:


path = 'D:/html_statement_analysis'


# In[4]:


def current_path():
    print("Current working directory before")
    print(os.getcwd())
    print()


# In[5]:


os.chdir(path)
current_path()


# In[6]:


def PD_from_Excel(file_name, ncolumns):
    df = pd.read_excel(file_name, index_col=0)
    df = df.iloc[:, 0:ncolumns]
    df = df.loc[df['Profit'].isnull() == False]
    df = df.loc[df['Open Time'].isnull() == False]
    df = df.loc[df['Profit'] != 'Profit']
    df_buysell = df.loc[df['Type'] != 'balance']
    df_fees = df.loc[df['Type'] == 'balance']
    df_buysell = df_buysell.loc[df_buysell['Close Time'].isnull() == False]
    df_buysell = df_buysell.loc[df_buysell['Close Time']  != ' ']
    dfr = pd.concat([df_buysell, df_fees])
    return dfr


# In[31]:


file_name = 'IF3730462.xlsx'    
df0 = PD_from_Excel(file_name, 13)
df0


# In[28]:


file_name = 'IF3730474.xlsx'    
df = PD_from_Excel(file_name, 13)
df


# In[29]:


df = pd.concat([df0, df])


# In[13]:


df


# In[14]:


df_broker_fees = df.loc[df['Type'] == 'balance']
TotalCustodyFees = df_broker_fees['Profit'].sum()
print(TotalCustodyFees)


# In[15]:


df_strategy_transactions = df.loc[df['Type'] != 'balance']


# In[57]:


df_strategy_transactions


# In[16]:


df_strategy_transactions=df_strategy_transactions.sort_values(by='Close Time', ascending=True)
print(df_strategy_transactions)


# In[17]:


TotalProfit = df_strategy_transactions['Profit'].sum()
print(TotalProfit)


# In[20]:


def equity_plot(pd_df,column_name):
    v = np.asarray(pd_df[column_name])
    plt.plot(np.cumsum(v))    


# In[21]:


df_broker_fees['Close Time'] = np.asarray(df_broker_fees['Open Time'])
df_broker_fees


# In[22]:


df = pd.concat([df_broker_fees, df_strategy_transactions])


# In[23]:


df=df.sort_values(by='Close Time', ascending=True)


# In[24]:


df


# In[25]:


equity_plot(df,'Profit')


# In[26]:


equity_plot(df_strategy_transactions,'Profit')


# In[86]:


df.to_excel("final_output.xlsx") 


# In[ ]:




